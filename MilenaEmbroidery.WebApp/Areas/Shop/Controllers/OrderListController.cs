﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MilenaEmbroidery.DTOs.Shop;
using MilenaEmbroidery.IServices.Shop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MilenaEmbroidery.WebApp.Areas.Admin.Controllers
{
    [Area("shop")]
    public class OrderListController : Controller
    {
        private readonly IOrderListService _orderListService;
        public OrderListController(IOrderListService orderListService)
        {
            _orderListService = orderListService;
        }

        public async Task<IActionResult> BasketForm()
        {
            IEnumerable<OrderListDTO> orderLists = Enumerable.Empty<OrderListDTO>();

            try
            {
                int? orderId = HttpContext.Session.GetInt32("OrderId");

                if (orderId == null)
                    throw new Exception("Order ID not found");

                orderLists = await _orderListService.GetByOrderId((int)orderId);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return View(orderLists.ToList());
        }

        public async Task<IActionResult> Update(IList<OrderListDTO> orderLists)
        {
            try
            {
                foreach (var item in orderLists)
                {
                    if (item.Quantity == 0)
                    {
                        bool check = await _orderListService.CheckIfExists(item.Id);

                        if (!check)
                            throw new Exception("Order list not exist.");

                        await _orderListService.Delete(item.Id);
                    }
                    else
                        await _orderListService.Update(item);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Delete(int id)
        {
            bool check = false;

            try
            {
                check = await _orderListService.CheckIfExists(id);

                if (!check)
                    throw new Exception("Order list not exist.");

                await _orderListService.Delete(id);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.GetBaseException().Message);
            }

            return RedirectToAction("BasketForm");
        }
    }
}
